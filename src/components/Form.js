import React, { useState } from "react";
import SignUpInfo from "./SignUpInfo";
import PersonalInfo from "./PersonalInfo";
import OtherInfo from "./OtherInfo";
import Success from "./Success";

export default function Form() {
  const [abc, setAbc] = useState(false);
  const [page, setPage] = useState(0);
  const [formData, setFormData] = useState({
    email: "",
    password: "",
    // confirmPassword: "",
    firstName: "",
    lastName: "",
    userName: "",
    nationality: "",
    other: "",
  });

  const formTitles = ["Sign up", "Personal Info", "Other"];

  const pageDisplay = () => {
    if (page === 0) {
      return <SignUpInfo formData={formData} setFormData={setFormData} />;
    } else if (page === 1) {
      return <PersonalInfo formData={formData} setFormData={setFormData} />;
    } else if (page === 2) {
      return <OtherInfo formData={formData} setFormData={setFormData} />;
    } else {
      return <Success />;
    }
  };

  function loginHandle(formData) {
    var isError = 0;
    if (page == 0) {
      if (
        formData.userName.length < 3 ||
        formData.password.length < 3
        // formData.confirmPassword.length < 3
      ) {
        isError = 1;
      }
    } else if (page == 1) {
      if (
        formData.firstName.length < 3 ||
        formData.lastName.length < 3 ||
        formData.email.length < 3
      ) {
        isError = 1;
      }
    } else if (page == 2) {
      if (formData.nationality.length < 3 || formData.other.length < 3) {
        isError = 1;
      }
    }
    if (isError == 0) {
      return true;
    } else {
      return false;
    }
  }

  console.log(loginHandle(formData));
  return (
    <div className="form">
      <div className="progressBar">
        <div
          style={{
            width:
              page === 0
                ? "25%"
                : page === 1
                ? "50%"
                : page === 2
                ? "75%"
                : "100%",
          }}
        ></div>
      </div>
      <div className="from-container">
        <div className="header">
          <h1>{formTitles[page]}</h1>
        </div>
        <form className="main-form">
          <div className="form-body-holder">
            <div className="form-body">{pageDisplay()}</div>
          </div>
          <div style={{ minHeight: "40px", display: page === 3 ? "none" : "" }}>
            <span className={"error-msg " + (abc ? "show" : " ")}>
              Minimum 3 charecter mandatory!
            </span>
          </div>
          <div className="footer" style={{ display: page === 3 ? "none" : "" }}>
            <button
              type="button"
              disabled={page === 0}
              onClick={() => {
                setPage((currentPage) => currentPage - 1);
              }}
            >
              Prev
            </button>
            <button
              type="button"
              onClick={() => {
                if (loginHandle(formData)) {
                  setPage((currentPage) => currentPage + 1);
                  setAbc(false);
                } else {
                  setAbc(true);
                }
              }}
            >
              {page === formTitles.length - 1 ? "Submit" : "Next"}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
