import React from "react";

export default function Success() {
  return (
    <div className="success-info">
      <h2>Thank you for filling out your information!</h2>
      <div className="header">
        <h1>Log in</h1>
      </div>
      <div className="login-container">
        <input type="text" placeholder="Username..." />
        <input type="password" placeholder="Password" />
      </div>
      <div style={{ minHeight: "40px" }}>
        <span className={"error-msg "}>Wrong user or password!</span>
      </div>
      <div className="footer">
        <button type="button">Log in</button>
      </div>
    </div>
  );
}
