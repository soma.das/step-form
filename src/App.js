import "./App.css";
import Form from "./components/Form";

function App() {
  return (
    <span className="App">
      <Form />
    </span>
  );
}

export default App;
